# Optimisation linéaire

Consiste à optimiser un score dépendant de plusieurs variables, aussi appelée fonction de coût.  
Le fichier `simplexe.py` contient le nécessaire pour trouver le maximum de la fonction suivante :

$`\large F(x) = \sum c_{ij} x_{ij}`$

où :
 - $`x_{ij}`$ vaut `1` si l'élève $`i`$ est affecté à l'appro $`j`$, `0` sinon
 - $`c_{ij}`$ est un coefficient à définir

Le problème consiste donc à trouver des coefficients permettant de trouver une bonne solution.  

Par exemple, $`c_{ij}`$ peut représenter la satisfaction (entre 0 et 1) de l'élève $`i`$ à rejoindre l'appro $`j`$.  
Le résultat sera la matrice des $`x_{ij}`$ étant alors la solution qui répartit les élèves dans les appros de sorte que la somme des satisfactions de tous les élèves soit maximale.  

Concrètement, il n'y a que la toute dernière partie du script à modifier pour effectuer des tests.  
Il faut donc créer un `numpy array` appelé `C` de taille `(n, 5)` où `n` est le nombre d'élève, `5` est le nombre d'approfondissements.  
Il faut également fournir un nombre de places disponible dans chaque appro, appelé `p` dans le script.  

Pour récupérer `F` le score maximum possible, et `X` le tableau de taille `(n, 5)` contenant les solutions $`x_{ij}`$, il suffit d'écrire `X, F = maximiser(C, p)`. Le script utilise la méthode du simplexe pour résoudre le problème.
