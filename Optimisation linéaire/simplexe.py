import numpy as np
from random import random
from pymprog import *

# ~~~~~~~~~~~~~~~~ Procédure de résolution ~~~~~~~~~~~~~~~~ #

def simplexe(c, A_eq = None, b_eq = None, A_ineq = None, b_ineq = None) :
    n = c.shape[0]  # Nombre de variables

    begin("Affectation des appros")
    x = var("x", n, kind=bool)
    maximize(sum(c[i]*x[i] for i in range(n)))

    if A_ineq is not None and b_ineq is not None :
        for i in range(A_ineq.shape[0]) :
            sum(A_ineq[i,j]*x[j] for j in range(n)) <= b_ineq[i]

    if A_eq is not None and b_eq is not None :
        for i in range(A_eq.shape[0]) :
            sum(A_eq[i,j]*x[j] for j in range(n)) == b_eq[i]

    solver("simplex")
    solve()

    F, X = None, None

    if status() == 6 :
        print("Maximum inexistant (fonction non bornée)")

    elif status() == 5 :
        F = vobj()
        print("Maximum trouvé :", F)
        X = np.zeros(n)
        for i in range(n):
            X[i] = x[i].primal

    end()
    return X, F

def maximiser(C, p: int) :
    """
    C : matrice des coefficients pour chaque élève et appro
    ( C[i,j] = coefficient pour l'élève i à l'appro j )
    
    p : nombre de places dans chaque appro
    ---
    Renvoie (X, F), tels que :
        X[i,j] = 1 si l'élève i se retrouve dans l'appro j,
                 0 sinon
        F = somme de tous les C[i,j]*X[i,j]
        F est maximal
    """
    if len(C.shape) != 2 or C.shape[1] != 5 :
        raise ValueError("C doit être une matrice à deux dimensions et 5 colonnes.")
    n = C.shape[0]
    c = C.reshape([5*n])
    A_eq = np.zeros([n, 5*n])
    for i in range(n) :
        A_eq[i, 5*i:5*(i+1)] = 1

    b_eq = np.ones([n])

    A_ineq = np.zeros([5, 5*n])
    for j in range(5) :
        for i in range(n) :
            A_ineq[j, 5*i+j] = 1
    b_ineq = p*np.ones([5])

    X, F = simplexe(c, A_eq, b_eq, A_ineq, b_ineq)
    if X is None : print("Pas de bol, pas de solution trouvée !")
    else : return X.reshape([n, 5]), F
    return X, F

def show(X, details=False) :
    for j in range(5) :
        appro = []
        for i in range(n) :
            if X[i,j] : appro.append(i)
        print("----------------------")
        print("Appro "+str(j)+" ("+str(len(appro))+" élèves) :")
        if details :
            print("Élève\tCoefficient")
            for i in appro : print("  "+str(i)+"\t"+str(C[i,j]))
        else :
            print(appro)

# ~~~~~~~~~~~~~~~~ Définition des coefficients ~~~~~~~~~~~~~~~~ #

n = 60  # Nombre d'élèves
p = 12  # Nombre de places par appro

C = np.zeros([n, 5])
for i in range(n) :
    for j in range(5) :
        C[i,j] = random()
    C[i,:] /= C[i,:].max()

X, F = maximiser(C, p)
show(X, details=True)
