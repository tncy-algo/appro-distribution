# Appro distribution

Dépôt d'essais d'algorithmes permettant de répartir les élèves de TELECOM Nancy dans les cinq approfondissements.

1. [**Résolution par mariage stable**](Mariages%20stables/) (algorithme de Gale-Shapley)
2. [**Résolution par optimisation linéaire**](Optimisation%20linéaire/) (algorithme du simplexe)
