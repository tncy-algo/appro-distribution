#!/usr/bin/python3
import csv
import argparse

# Styles d'affichage dans la console
NORMAL = "\033[0m"
BOLD   = "\033[1m"
PURPLE = "\033[35m"

# ~~~~~~~~~~~~~~~~~~~~~~~~~~ Parsing de la commande ~~~~~~~~~~~~~~~~~~~~~~~~~~ #

parser = argparse.ArgumentParser(
    usage=("./gale-shapley.py -r RANKING_CSV -c CHOICES_CSV "
           "[--help | -n NB_PLACES | --output FILENAME | --verbose]"),
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    epilog=(BOLD + "Exemple :\r\n " + NORMAL + "./gale-shapley.py "
            "-r sample/random_ranking.csv \n-c sample/random_choices.csv")
)
parser.add_argument("-r", "--ranking_csv", default=None,
                    help=(f"CSV de {BOLD}classement des élèves{NORMAL} "
                          "par approfondissement"))
parser.add_argument("-c", "--choices_csv", default=None,
                    help=f"CSV des {BOLD}voeux{NORMAL} des élèves")
parser.add_argument("-n", type=int, default=None,
                    help="Nombre de places max de chaque approfondissement")
parser.add_argument("-o", "--output", default="distribution.csv",
                    help="CSV où sont exportés les résultats")
parser.add_argument("-v", "--verbose", action="count",
                    help="Afficher les élèves non placés à chaque tour")
args = parser.parse_args()

rankingpath = args.ranking_csv
choicespath = args.choices_csv
nb_places = args.n
def verbose(*fargs):
    if args.verbose:
        print(*fargs)

if rankingpath is None or choicespath is None:
    parser.print_help()
    exit(1)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~ Chargement des fichiers ~~~~~~~~~~~~~~~~~~~~~~~~~ #

# Classement des approfondissements
# (i.e. le choix des appros)
with open(rankingpath, 'r') as f:
    reader = csv.reader(f)
    rows = list(reader)
A = rows.pop(0)
choixA = {A[i]: [row[i] for row in rows] for i in range(len(A))}

# Voeux des élèves
# (i.e. les choix des élèves)
with open(choicespath, 'r') as f:
    reader = csv.reader(f)
    rows = list(reader)
E = [row[0] for row in rows]
choixE = {E[i]: rows[i][1:] for i in range(len(rows))}

if nb_places is None:
    # si le nombre de places n'est pas défini, on choisit
    # l'arrondi à l'entier supérieur du rapport
    # [nombre d'élèves] / [nombre d'appros]
    nb_places = -int(-len(E)/len(A))


# ~~~~~~~~~~~~~~~~~~~~~~~~~ Exécution de l'algorithme ~~~~~~~~~~~~~~~~~~~~~~~~ #

# Relation de préférence d'une appro entre deux élèves
def prefere(appro, eleve1, eleve2):
    return choixA[appro].index(eleve1) < choixA[appro].index(eleve2)

distrib = {"IAMD": [], "IL": [], "ISS": [], "LE": [], "SIE": []}
sans_appro = set(E)

verbose(BOLD + "Élèves sans approfondissement à chaque tour :" + NORMAL)

t = 0
while sans_appro:
    t += 1
    verbose("Tour n°" + str(t))
    rejetes = set()     # ensemble des élèves rejetés

    for eleve in sans_appro:
        verbose("  " + eleve)
        appro = choixE[eleve].pop(0)    # appro en premier choix restant

        # On range l'élève dans l'appro en le classant par rapport aux autres
        for i in range(len(distrib[appro]) + 1):
            if i == len(distrib[appro]) or prefere(appro, eleve, distrib[appro][i]):
                distrib[appro].insert(i, eleve)
                break

        # S'il y a trop de monde, on retire le dernier du classement
        if len(distrib[appro]) > nb_places:
            rejetes.add(distrib[appro].pop(-1))

    sans_appro = rejetes

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Résultats ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

for appro in A:
    verbose("\n" + BOLD + appro + NORMAL)
    for eleve in distrib[appro]:
        verbose("  " + eleve)

f = open(args.output, "w")
f.write(",".join(A) + "\n")
for i in range(nb_places):
    line = []
    for appro in A:
        if i < len(distrib[appro]): line.append(distrib[appro][i])
        else: line.append("")
    f.write(",".join(line) + "\n")
f.close()
print("\n" + BOLD + PURPLE + f"Résultats exportés vers {args.output}" + NORMAL)
