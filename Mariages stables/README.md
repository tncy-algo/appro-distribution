# Distribution des approfondissements par mariage stable

## `gale-shapley.py`

Adaptation de l'algorithme de [Gale-Shapley](https://fr.wikipedia.org/wiki/Algorithme_de_Gale_et_Shapley) pour la distribution des élèves dans les différents approfondissements.  
Deux fichiers doivent être donnés en entrée :  
 - Un CSV des classements des élèves par approfondissement *(ranking)*
 - Un CSV des classements des voeux des élèves *(choices)*

**Exemple :**
```shell
$ ./gale-shapley.py -r sample/random_ranking.csv -c sample/random_choices.csv --verbose
```

Des exemples de fichiers sont disponibles dans le répertoire `sample`.  
Le résultat de la distribution est sauvegardée par défaut dans le fichier `distribution.csv`.
